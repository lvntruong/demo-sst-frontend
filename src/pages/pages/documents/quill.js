import { useState, useEffect } from "react";
import QuillEditor, { Quill } from "react-quill";
import "react-quill/dist/quill.snow.css"; // ES6
import { defaultText } from "./constants";
import HeadingFormat from "./heading/heading-format";
import QuillGfxModule from "./gfx/gfx-module";
import QuillLivexFormat from "./gfx/livex-format";
import "./gfx/style.css";
import _ from "lodash";
import { v4 as uuidv4 } from "uuid";
import { API } from "aws-amplify";
import { CONTENT_TYPES, VERSIONS, LANGUAGES, CONTEXT, modalStyle } from "../../dashboards/app";

import {
  Button,
  ButtonGroup,
  Container,
  Grid,
  IconButton,
  Popover,
  TextField,
  Tooltip,
  tooltipClasses,
  Modal,
  Box,
} from "@mui/material";
import SuiBox from "components/SuiBox";
import SuiSelect from "components/SuiSelect";
import SuiTypography from "components/SuiTypography";
import SuiInput from "components/SuiInput";
import SuiButton from "components/SuiButton";

Quill.register("formats/heading", HeadingFormat);
Quill.register("modules/gfx", QuillGfxModule);
Quill.register("formats/livex", QuillLivexFormat);

const Parchment = Quill.import("parchment");

function CustomQuill() {
  const searchParams = new URLSearchParams(window.location.search);
  const customId = searchParams.get("id");

  let ref;
  const [text, setText] = useState("");
  const [contents, setContents] = useState([]);
  const [values, setContentNode] = useState({});
  const [newContent, setNewContent] = useState({});
  const [openModal, setOpenModal] = useState(false);
  const [openModalAddContent, setOpenModalAddContent] = useState(false);
  const [documentId, setDocumentId] = useState(customId || localStorage.getItem("docId"));

  useEffect(() => {
    fetchContents();
    document.addEventListener("contextmenu", handleRightClick);
    window.addEventListener("mousedown", onMouseDown);
    if (documentId) {
      fetchDocumentById(documentId);
    } else {
      createDocument();
      setText(defaultText);
    }

    return () => {
      window.removeEventListener("mousedown", onMouseDown);
      document.removeEventListener("contextmenu", handleRightClick);
    };
  }, []);

  const fetchContents = () => {
    return API.get("pages", "/contents").then((data) => {
      setContents(data || []);
    });
  };

  const fetchDocumentById = (id) => {
    return API.get("pages", `/docs/${id}`).then((data) => {
      // setText(data.body);
      window.editorRef.clipboard.dangerouslyPasteHTML(data.body, "api");
    });
  };

  const updateDocumentById = (id, body) => {
    return API.put("pages", `/docs/${id}`, {
      body: {
        body,
      },
    });
  };

  const createDocument = () => {
    return API.post("pages", "/docs", { body: {} }).then((data) => {
      localStorage.setItem("docId", data._id);
      setDocumentId(data._id);
    });
  };

  useEffect(() => {
    if (ref && typeof ref.getEditor !== "undefined") {
      //  editorRef = ref.getEditor();
      window.editorRef = ref.getEditor();
      // console.log(editorRef);
      // editorRef.enable(false)
    }
  }, [ref]);

  const handleAddNewContent = () => {
    API.post("pages", "/contents", { body: newContent }).then((data) => {
      // setOpenModalAddContent(false);
      window.location.reload();
    });
  };

  const handleRightClick = (e) => {
    console.log(e.target);
  };

  const onMouseDown = (e) => {
    let gfx;
    if (e.which !== 3) {
      // not right click
      const elementClassList = _.get(e, "target.classList", []);
      console.log({ elementClassList });

      if (_.includes(elementClassList, "livex-quill-gfx")) {
        // gfx
        // show modal
        // console.log(e.target.dataset['content']);
        window.currentGFX = e.target;
        const blot = Parchment.find(e.target);
        if (blot) {
          try {
            gfx = JSON.parse(blot.domNode.getAttribute("data-content"));
            console.log(gfx.payload);
            setContentNode(gfx.payload);
            setOpenModal(true);
          } catch (e) {}
        }
      }
    }
  };

  const _updateDocumentById = _.debounce(updateDocumentById, 1000);

  const onEditorChange = async (content, delta, source, editor) => {
    console.log({ content, delta, source, editor });
    _updateDocumentById(documentId, content);
  };

  const onInsertHeading = async () => {
    const selection = window.editorRef.getSelection();
    insertHeading(selection);
  };

  const updateEditorContent = (delta, source = "user") => {
    if (!window.editorRef) {
      return;
    }
    window.editorRef.updateContents(delta, source);
  };

  const insertHeading = (range) => {
    const currentSelectedFormat = window.editorRef.getFormat(range);
    if (range.length > 0) {
      window.editorRef.insertText(range.index, "\n", "user");
      window.editorRef.insertText(range.index + range.length + 1, "\n", "user");
      const delta = window.editorRef.formatLine(
        range.index + 1,
        range.length,
        "heading",
        currentSelectedFormat.heading ? false : true,
        "user"
      );
      updateEditorContent(delta, "user");
    } else {
      const delta = window.editorRef.format(
        "heading",
        currentSelectedFormat.heading ? false : true,
        "user"
      );
      updateEditorContent(delta, "user");
    }
  };

  const handleOnGfxCardSubmit = (content) => {
    const gfx = content;
    const range = window.editorRef.getSelection();
    console.log(range);

    if (!_.get(gfx, "id")) {
      gfx.id = uuidv4();
    }

    let delta = {
      ops: [],
    };

    const selectIndex = _.get(range, "index", 1);
    // remove current gfx if length > 0
    if (_.get(range, "length", 0) > 0) {
      delta.ops.push({
        retain: selectIndex > 0 ? selectIndex : 1,
      });

      delta.ops.push({
        retain: range.length,
        attributes: {
          livex: false,
        },
      });

      updateEditorContent(delta, "user");
      // End remove current gfx
    }

    // Begin add gfx
    delta = {
      ops: [],
    };

    let objectOps = {
      retain: selectIndex >= 0 ? selectIndex : 0,
    };

    // Handle first character ignored
    if (selectIndex === 0) objectOps["insert"] = "";

    delta.ops.push(objectOps);

    //@todo Idea: if user not select a text, that mean length = 0, we need to add a space like " "
    if (_.get(range, "length", 0) < 1) {
      delta.ops.push({
        insert: gfx.value,
        attributes: {
          livex: {
            payload: gfx,
            type: "gfx",
          },
        },
      });
    } else {
      delta.ops.push({
        retain: range.length > 0 ? range.length : 1,
        attributes: {
          livex: {
            payload: gfx,
            type: "gfx",
          },
        },
      });
    }
    // console.log(delta);
    updateEditorContent(delta, "user");
  };

  const handleUpdateContent = () => {
    window.currentGFX.innerHTML = values.value;
    window.currentGFX.dataset["content"] = JSON.stringify({ payload: values, type: "gfx" });
    setOpenModal(false);
  };

  // const documentLink = customId ? window.location.href : window.location.href + "?id=" + documentId;
  return (
    <div>
      <QuillEditor
        ref={(_ref) => (ref = _ref)}
        //value={text}
        onChange={onEditorChange}
        modules={{
          toolbar: [
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            ["bold", "italic", "underline"],
          ],
        }}
      />
      <br />
      <hr />
      {contents.map((content, index) => {
        return (
          <Tooltip
            title={`Content type: ${content.contentType}, language:  ${content.language}, context: ${content.context}, version: ${content.version}`}
          >
            <Button
              onClick={() => handleOnGfxCardSubmit(content)}
              draggable
              key={content._id}
              onDragStart={(ev) => {
                ev.dataTransfer.setData("node", JSON.stringify({ ...content, type: "content" }));
              }}
            >
              {content.value}
            </Button>
          </Tooltip>
        );
      })}
      <hr />
      <br />
      <SuiButton
        component="button"
        rel="noreferrer"
        variant="outlined"
        color="dark"
        onClick={() => setOpenModalAddContent(true)}
      >
        Add new content
      </SuiButton>
      {/* <br />
      <br />
      <hr />
       <a href={documentLink} target="_blank" rel="noreferrer">
        Share document link: {documentLink}
      </a> */}
      <Modal
        open={openModal}
        onClose={() => setOpenModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <SuiBox display="flex">
            {/* <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Content name
                </SuiTypography>
              </SuiBox>
              <SuiInput
                value={values.value}
                onChange={({ target: { value } }) => {
                  setContentNode({ ...values, value: value });
                }}
              />
            </div> */}
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Type
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  CONTENT_TYPES.find((type) => type.value === values.contentType) || {
                    label: values.contentType,
                    value: values.contentType,
                  }
                }
                options={CONTENT_TYPES}
                onChange={(type) => {
                  // handleChangeNode("contentType", type.value);
                  setContentNode({ ...values, contentType: type.value });
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Language
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  LANGUAGES.find((type) => type.value === values.language) || {
                    label: values.language,
                    value: values.language,
                  }
                }
                options={LANGUAGES}
                onChange={(type) => {
                  // handleChangeNode("language", type.value);
                  setContentNode({ ...values, language: type.value });
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Context
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  CONTEXT.find((type) => type.value === values.context) || {
                    label: values.context,
                    value: values.context,
                  }
                }
                options={CONTEXT}
                onChange={(type) => {
                  // handleChangeNode("context", type.value);
                  setContentNode({ ...values, context: type.value });
                }}
              />
            </div>
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Version
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  VERSIONS.find((type) => type.value === values.version) || {
                    label: values.version,
                    value: values.version,
                  }
                }
                options={VERSIONS}
                onChange={(type) => {
                  // handleChangeNode("version", type.value);
                  setContentNode({ ...values, version: type.value });
                }}
              />
            </div>
          </SuiBox>
          <SuiBox display="flex" justifyContent="center">
            <SuiButton
              component="button"
              rel="noreferrer"
              variant="outlined"
              color="dark"
              onClick={handleUpdateContent}
              style={{ marginTop: 10 }}
            >
              Save
            </SuiButton>
          </SuiBox>
        </Box>
      </Modal>
      <Modal
        open={openModalAddContent}
        onClose={() => setOpenModalAddContent(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <SuiBox display="flex">
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Content Type
                </SuiTypography>
              </SuiBox>
              <SuiInput
                placeholder=""
                onChange={({ target: { value } }) => {
                  setNewContent({ ...newContent, contentType: value });
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Language
                </SuiTypography>
              </SuiBox>
              <SuiInput
                placeholder=""
                onChange={({ target: { value } }) => {
                  setNewContent({ ...newContent, language: value });
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Context
                </SuiTypography>
              </SuiBox>
              <SuiInput
                placeholder=""
                onChange={({ target: { value } }) => {
                  setNewContent({ ...newContent, context: value });
                }}
              />
            </div>
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Version
                </SuiTypography>
              </SuiBox>
              <SuiInput
                placeholder=""
                onChange={({ target: { value } }) => {
                  setNewContent({ ...newContent, version: value });
                }}
              />
            </div>
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Content name
                </SuiTypography>
              </SuiBox>
              <SuiInput
                placeholder=""
                onChange={({ target: { value } }) => {
                  setNewContent({ ...newContent, value });
                }}
              />
            </div>
          </SuiBox>
          <SuiButton
            component="button"
            rel="noreferrer"
            variant="outlined"
            color="dark"
            onClick={handleAddNewContent}
          >
            Save
          </SuiButton>
        </Box>
      </Modal>
    </div>
  );
}

export default CustomQuill;
