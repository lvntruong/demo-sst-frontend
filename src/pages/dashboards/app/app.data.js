module.exports = [
  {
    id: `page_1`,
    type: "page",
    url: "/page_1",
    children: [
      {
        type: "container",
        id: "page_1_c_1",
        children: [
          {
            type: "section",
            id: "page_1_c_1_s_1",
            children: [
              {
                type: "content",
                id: "page_1_c_1_s_1_C1",
                value: `Content`,
              },
            ],
          },
          {
            type: "section",
            id: "page_1_c_1_s_2",
          },
        ],
      },
      {
        type: "container",
        id: "page_1_c_2",
      },
      {
        type: "container",
        id: "page_1_c_2",
      },
    ],
  },
];
