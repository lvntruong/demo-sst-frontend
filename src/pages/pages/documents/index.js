// @mui material components
import Grid from "@mui/material/Grid";

// Composite Content React components
import SuiBox from "components/SuiBox";

// Composite Content React example components
import DashboardLayout from "components/LayoutContainers/DashboardLayout";
import DashboardNavbar from "components/Navbars/DashboardNavbar";
import Quill from "./quill";

function Notifications() {

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SuiBox mt={6} mb={3}>
        <Grid container spacing={3} justifyContent="center">
          <Grid item xs={12} lg={8}>
            <Quill />
          </Grid>
        </Grid>
      </SuiBox>
    </DashboardLayout>
  );
}

export default Notifications;
