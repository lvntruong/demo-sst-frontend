import { Quill } from "react-quill";
import _ from "lodash";

const Block = Quill.import("blots/block");

class Heading extends Block {
  static create(options) {
    const payload = _.get(options, "payload");

    let node = super.create(payload);

    let classes = ["xxx-content"];

    node.setAttribute("class", _.join(classes, " "));

    return node;
  }

  static formats(domNode) {
    return domNode.className === "xxx-content";
  }

  format(name, value) {
    super.format(name, value);
  }
}

Heading.blotName = "heading";
Heading.tagName = "div";

export default Heading;
