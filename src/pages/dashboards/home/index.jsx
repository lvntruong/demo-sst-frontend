import DashboardLayout from "components/LayoutContainers/DashboardLayout";
import React, { useEffect, useRef } from "react";
import ReactQuill from "react-quill";
import Quill from "quill";

const Home = () => {
  const editorRef = useRef();
  useEffect(() => {
    if (editorRef.current.children.length === 0) {
      const quill = new Quill(editorRef.current, {
        modules: {
          toolbar: [
            [{ header: [1, 2, false] }],
            ["bold", "italic", "underline"],
            ["image", "code-block"],
          ],
        },
        placeholder: "Compose an epic...",
        theme: "snow", // or 'bubble',
        formats: ["background", "background", "color"],
      });

      //   const value = `<button> click here</button>`;
      //   const delta = quill.clipboard.convert(value);
      //   quill.root.innerHTML = `<button>click here </button>`;
    }
  }, [editorRef]);
  return (
    <DashboardLayout>
      <h1>Home</h1>
      <div ref={editorRef} style={{ minHeight: 400 }}></div>
      <ReactQuill
        modules={{
          toolbar: [
            [{ header: [1, 2, false] }],
            ["bold", "italic", "underline"],
            ["image", "code-block"],
          ],
        }}
        value={`<button>12321</button>`}
      />
    </DashboardLayout>
  );
};

export default Home;
