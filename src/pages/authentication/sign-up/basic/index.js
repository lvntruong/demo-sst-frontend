/**
=========================================================
* Composite Content React - v3.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

import { useState } from "react";

// react-router-dom components
import { Link } from "react-router-dom";

// @mui material components
import Card from "@mui/material/Card";
import Checkbox from "@mui/material/Checkbox";

// Composite Content React components
import SuiBox from "components/SuiBox";
import SuiTypography from "components/SuiTypography";
import SuiInput from "components/SuiInput";
import SuiButton from "components/SuiButton";

// Authentication layout components
import BasicLayout from "pages/authentication/components/BasicLayout";
import Socials from "pages/authentication/components/Socials";
import Separator from "pages/authentication/components/Separator";

// Images
import curved6 from "assets/images/curved-images/curved6.jpg";
import { Auth } from "aws-amplify";

function Basic() {
  const [agreement, setAgremment] = useState(true);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSetAgremment = () => setAgremment(!agreement);

  const handleSubmit = async (event) => {
    event.preventDefault();

    // setIsLoading(true);

    try {
      const newUser = await Auth.signUp({
        username: email,
        password: password,
      });
      const confirmationCode = window.prompt("Please enter confirmation code");
      handleConfirmationSubmit(confirmationCode);
      // setIsLoading(false);
      // setNewUser(newUser);
    } catch (e) {
      alert(e.message);
      // onError(e);
      // setIsLoading(false);
    }
  };

  async function handleConfirmationSubmit(confirmationCode) {
    // setIsLoading(true);

    try {
      await Auth.confirmSignUp(email, confirmationCode);
      // alert("Success!");
      await Auth.signIn(email, password);

      // userHasAuthenticated(true);
      // history.push("/authentication/sign-in/basic");
      window.location.href("/dashboards/app-pages");
    } catch (e) {
      alert(e.message);

      // onError(e);
      // setIsLoading(false);
    }
  }

  return (
    <BasicLayout
      title="Welcome!"
      description="Use these awesome forms to login or create new account in your project for free."
      image={curved6}
    >
      <Card>
        <SuiBox p={3} mb={1} textAlign="center">
          <SuiTypography variant="h5" fontWeight="medium">
            Register with
          </SuiTypography>
        </SuiBox>
        <SuiBox mb={2}>
          <Socials />
        </SuiBox>
        <Separator />
        <SuiBox pt={2} pb={3} px={3}>
          <SuiBox component="form" role="form">
            {/* <SuiBox mb={2}>
              <SuiInput placeholder="Name" />
            </SuiBox> */}
            <SuiBox mb={2}>
              <SuiInput
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </SuiBox>
            <SuiBox mb={2}>
              <SuiInput
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </SuiBox>
            <SuiBox display="flex" alignItems="center">
              <Checkbox checked={agreement} onChange={handleSetAgremment} />
              <SuiTypography
                variant="button"
                fontWeight="regular"
                onClick={handleSetAgremment}
                sx={{ cursor: "pointer", userSelect: "none" }}
              >
                &nbsp;&nbsp;I agree the&nbsp;
              </SuiTypography>
              <SuiTypography component="a" href="#" variant="button" fontWeight="bold" textGradient>
                Terms and Conditions
              </SuiTypography>
            </SuiBox>
            <SuiBox mt={4} mb={1}>
              <SuiButton variant="gradient" color="dark" fullWidth onClick={handleSubmit}>
                sign up
              </SuiButton>
            </SuiBox>
            <SuiBox mt={3} textAlign="center">
              <SuiTypography variant="button" color="text" fontWeight="regular">
                Already have an account?&nbsp;
                <SuiTypography
                  component={Link}
                  to="/authentication/sign-in/basic"
                  variant="button"
                  color="dark"
                  fontWeight="bold"
                  textGradient
                >
                  Sign in
                </SuiTypography>
              </SuiTypography>
            </SuiBox>
          </SuiBox>
        </SuiBox>
      </Card>
    </BasicLayout>
  );
}

export default Basic;
