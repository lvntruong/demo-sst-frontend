/**
=========================================================
* Composite Content React - v3.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-dashboard-pro-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import { useMemo } from "react";

// @mui material components
import Grid from "@mui/material/Grid";

// Composite Content React components
import SuiBox from "components/SuiBox";

// Composite Content React example components
import DashboardLayout from "components/LayoutContainers/DashboardLayout";
import DashboardNavbar from "components/Navbars/DashboardNavbar";
import Footer from "components/Footer";
import EventCalendar from "components/Calendar";

// Calendar application components
import Header from "pages/applications/calendar/components/Header";
import NextEvents from "pages/applications/calendar/components/NextEvents";
import ProductivityChart from "pages/applications/calendar/components/ProductivityChart";

// Data
import calendarEventsData from "pages/applications/calendar/data/calendarEventsData";

function Calendar() {
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <SuiBox pt={3}>
        <SuiBox display="flex" justifyContent="flex-end" mt={1} mb={4} mx={2}>
          <Header />
        </SuiBox>
        <Grid container spacing={3}>
          <Grid item xs={12} xl={9} sx={{ height: "max-content" }}>
            {useMemo(
              () => (
                <EventCalendar
                  initialView="dayGridMonth"
                  initialDate="2021-08-10"
                  events={calendarEventsData}
                  selectable
                  editable
                />
              ),
              [calendarEventsData]
            )}
          </Grid>
          <Grid item xs={12} xl={3}>
            <SuiBox mb={3}>
              <NextEvents />
            </SuiBox>
            <SuiBox mb={3}>
              <ProductivityChart />
            </SuiBox>
          </Grid>
        </Grid>
      </SuiBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Calendar;
