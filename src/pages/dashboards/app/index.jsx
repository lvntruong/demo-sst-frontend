import React, { useEffect, useRef, useState, useCallback } from "react";
import appPages from "./app.data";
import {
  Button,
  ButtonGroup,
  Container,
  Grid,
  IconButton,
  Popover,
  TextField,
  Tooltip,
  tooltipClasses,
  Modal,
  Box,
} from "@mui/material";
import { styled } from "@mui/material/styles";

import TreeView from "@mui/lab/TreeView";
import TreeItem from "@mui/lab/TreeItem";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";

import { cloneDeep, get, debounce } from "lodash";
import { API } from "aws-amplify";
import { v4 as uuidV4 } from "uuid";
import DeleteIcon from "@mui/icons-material/Delete";
import { Folder, FolderOpen } from "@mui/icons-material";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import SuiBox from "components/SuiBox";
import SuiSelect from "components/SuiSelect";
import SuiTypography from "components/SuiTypography";
import DashboardLayout from "components/LayoutContainers/DashboardLayout";
import SuiButton from "components/SuiButton";
import DashboardNavbar from "components/Navbars/DashboardNavbar";

export const CONTENT_TYPES = [
  {
    value: "type_1",
    label: "Type 1",
  },
  {
    value: "type_2",
    label: "Type 2",
  },
  {
    value: "type_3",
    label: "Type 3",
  },
  {
    value: "type_4",
    label: "Type 4",
  },
];

export const LANGUAGES = [
  {
    value: "en",
    label: "English",
  },
  {
    value: "es",
    label: "Spanish",
  },
  {
    value: "fra",
    label: "France",
  },
  {
    value: "vn",
    label: "Vietnam",
  },
];

export const CONTEXT = [
  {
    value: "context_1",
    label: "Context 1",
  },
  {
    value: "context_2",
    label: "Context 2",
  },
  {
    value: "context_3",
    label: "Context 3",
  },
  {
    value: "context_4",
    label: "Context 4",
  },
];

export const VERSIONS = [
  {
    value: "1.0",
    label: "Version 1.0",
  },
  {
    value: "2.0",
    label: "Version 2.0",
  },
  {
    value: "3.0",
    label: "Version 3.0",
  },
];

export const modalStyle = {
  position: "absolute",
  top: "30%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  minWidth: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const LayoutItem = ({ item, children, handleSelectPage }) => {
  return (
    <TreeItem
      onClick={() => {
        console.log("asdjhashjdgashdbs");
        handleSelectPage();
      }}
      label={<span style={{ display: "flex", alignItems: "center" }}> {item.type || "page"} </span>}
      nodeId={item._id}
    >
      {children}
    </TreeItem>
  );
};

const renderPageLayout = (items = [], handleSelectPage) => {
  return items.map((item) => {
    return (
      <LayoutItem item={item} handleSelectPage={handleSelectPage}>
        {renderPageLayout(item.children, handleSelectPage)}
      </LayoutItem>
    );
  });
};

const UIContainer = ({ children, type, handleDrop, allowDrop, deleteNodeElement }) => {
  return (
    <div
      style={{ border: "1px solid black", padding: 24, minHeight: 200 }}
      onDragOver={allowDrop}
      onDrop={handleDrop}
    >
      {deleteNodeElement}
      <Grid container>{children}</Grid>
    </div>
  );
};

const UISection = ({ children, type, handleDrop, allowDrop, deleteNodeElement }) => {
  return (
    <Grid
      item
      xs={6}
      style={{ border: "1px solid blue", padding: 8, minHeight: 100 }}
      onDragOver={allowDrop}
      onDrop={handleDrop}
    >
      {deleteNodeElement}
      {children}
    </Grid>
  );
};

const UIContent = ({ value, type, deleteNodeElement, node, handleUpdateTree, updatePage }) => {
  const [open, setModalVisibility] = useState(false);
  const [values, setContentNode] = useState({});

  const handleChangeNode = (name, value) => {
    setContentNode((prev) => {
      const newState = {
        ...prev,
        [name]: value,
      };
      // mutate the node
      Object.assign(node, newState);
      handleUpdateTree();
      return newState;
    });
  };

  useEffect(() => {
    setContentNode(node);
  }, [node]);

  const handleUpdateContent = () => {
    updatePage();
    setModalVisibility(false);
  };

  return (
    <>
      <div
        onClick={() => setModalVisibility(true)}
        style={{ border: "1px solid green", padding: 8 }}
      >
        {deleteNodeElement}
        {node.value}
      </div>
      <Modal
        open={open}
        onClose={() => {
          setModalVisibility(false);
        }}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <SuiBox display="flex">
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Type
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  CONTENT_TYPES.find((type) => type.value === values.contentType) || {
                    label: "",
                    value: "",
                  }
                }
                options={CONTENT_TYPES}
                onChange={(type) => {
                  handleChangeNode("contentType", type.value);
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Language
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  LANGUAGES.find((type) => type.value === values.language) || {
                    label: "",
                    value: "",
                  }
                }
                options={LANGUAGES}
                onChange={(type) => {
                  handleChangeNode("language", type.value);
                }}
              />
            </div>

            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Context
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  CONTEXT.find((type) => type.value === values.context) || {
                    label: "",
                    value: "",
                  }
                }
                options={CONTEXT}
                onChange={(type) => {
                  handleChangeNode("context", type.value);
                }}
              />
            </div>
            <div style={{ padding: 4 }}>
              <SuiBox mb={1} ml={0.5} lineHeight={0} display="inline-block">
                <SuiTypography
                  component="label"
                  variant="caption"
                  fontWeight="bold"
                  textTransform="capitalize"
                >
                  Version
                </SuiTypography>
              </SuiBox>
              <SuiSelect
                value={
                  VERSIONS.find((type) => type.value === values.version) || {
                    label: "",
                    value: "",
                  }
                }
                options={VERSIONS}
                onChange={(type) => {
                  handleChangeNode("version", type.value);
                }}
              />
            </div>
          </SuiBox>
          <SuiBox display="flex" justifyContent="center">
            <SuiButton
              component="button"
              rel="noreferrer"
              variant="outlined"
              color="dark"
              onClick={handleUpdateContent}
              style={{ marginTop: 10 }}
            >
              Save
            </SuiButton>
          </SuiBox>
        </Box>
      </Modal>
    </>
  );
};

const NODE_MAP = {
  container: UIContainer,
  section: UISection,
  content: UIContent,
};

const NodeComponent = ({ root, node, path = [], handleUpdateTree, updatePage }) => {
  const { children = [] } = node;
  const Node = NODE_MAP[node.type];

  const handleDrop = (ev) => {
    ev.preventDefault();
    const data = JSON.parse(ev.dataTransfer.getData("node"));
    if (!node.children) {
      node.children = [];
    }
    node.children.push({
      ...data,
      _id: uuidV4(),
    });
    handleUpdateTree();
    ev.stopPropagation();
  };

  const deleteNode = () => {
    const parentPath = path.slice(0, path.length - 1).join(".children.");
    const [nodePosition] = path.slice(path.length - 1);
    const parentNode = get(root.children, parentPath) || root;
    if (parentNode.children) {
      parentNode.children.splice(nodePosition, 1);
      handleUpdateTree();
    }
  };

  const allowDrop = (e) => {
    e.preventDefault();
  };

  const childrenElements = children.map((node, index) => (
    <NodeComponent
      key={node._id}
      root={root}
      node={node}
      path={path.concat(`${index}`)}
      handleUpdateTree={handleUpdateTree}
      updatePage={updatePage}
    />
  ));

  const deleteNodeElement = <DeleteIcon style={{ position: "relative" }} onClick={deleteNode} />;

  if (typeof Node === "function") {
    return (
      <Node
        node={node}
        path={path}
        handleDrop={handleDrop}
        allowDrop={allowDrop}
        deleteNodeElement={deleteNodeElement}
        handleUpdateTree={handleUpdateTree}
        updatePage={updatePage}
      >
        {childrenElements}
      </Node>
    );
  }
  return (
    <div
      onDragOver={allowDrop}
      onDrop={handleDrop}
      style={{ border: "1px dashed gray", minHeight: 600, padding: 4 }}
    >
      {childrenElements}
    </div>
  );
};

const renderPageContent = (rootNode, path = [], handleUpdateTree, updatePage) => {
  return (
    <NodeComponent
      root={rootNode}
      node={rootNode}
      path={path}
      handleUpdateTree={handleUpdateTree}
      updatePage={updatePage}
    />
  );
};

const ELEMENTS = [
  {
    type: "container",
  },
  {
    type: "section",
  },
];

const AppPages = () => {
  const [pages, setPages] = useState([]);
  const [contents, setContents] = useState([]);
  const [selectedPage, setSelectedPage] = useState({});
  const [pageURLs, setPageURLs] = useState({});

  const handleUpdateTree = () => {
    const newPage = cloneDeep(selectedPage);
    setSelectedPage(newPage);
  };

  const fetchPages = () => {
    return API.get("pages", "/pages").then((data) => {
      setPages(data || []);
    });
  };

  const fetchContents = () => {
    return API.get("pages", "/contents").then((data) => {
      setContents(data || []);
    });
  };

  const createPage = () => {
    return API.post("pages", "/pages", {
      body: {
        url: "/example",
        children: [],
      },
    });
  };

  const updatePage = () => {
    return API.put("pages", `/pages/${selectedPage._id}`, {
      body: selectedPage,
    });
  };

  const deletePage = (pageID) => {
    return API.del("pages", `/pages/${pageID}`);
  };

  useEffect(() => {
    fetchPages();
    fetchContents();
  }, []);

  const updateRoutePageById = (page, url) => {
    return API.put("pages", `/pages/${page._id}`, {
      body: {
        ...page,
        url,
      },
    });
  };

  const _updateRoutePageById = useCallback(debounce(updateRoutePageById, 500), []);

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <Container maxWidth="xl">
        <Grid container>
          <Grid item xs={3}>
            <Button
              onClick={() => {
                createPage().then(() => {
                  fetchPages();
                });
              }}
            >
              New Page
            </Button>
            <TreeView
              aria-label="file system navigator"
              defaultCollapseIcon={<ExpandMoreIcon />}
              defaultExpandIcon={<ChevronRightIcon />}
              sx={{ minHeight: 340, flexGrow: 1, maxWidth: 400, overflowY: "auto" }}
            >
              {pages.map((page) => {
                const url = pageURLs[page._id] || page.url;
                return (
                  <TreeItem
                    key={page._id}
                    onClick={() => {
                      setSelectedPage(page);
                    }}
                    label={
                      <span style={{ display: "flex", alignItems: "center" }}>
                        page
                        <TextField
                          value={url}
                          onChange={(e) => {
                            setPageURLs((prev) => ({ ...prev, [page._id]: e.target.value }));
                            _updateRoutePageById(page, e.target.value);
                          }}
                          style={{ marginLeft: 4, maxWidth: 130 }}
                        />
                        <DeleteIcon
                          style={{ marginLeft: 8 }}
                          onClick={() => {
                            deletePage(page._id).then(() => fetchPages());
                          }}
                        />
                      </span>
                    }
                    nodeId={page._id}
                    collapseIcon={<Folder />}
                    expandIcon={<FolderOpen />}
                  >
                    {renderPageLayout(page.children, () => {
                      setSelectedPage(page);
                    })}
                  </TreeItem>
                );
              })}
            </TreeView>
          </Grid>
          <Grid item xs={7}>
            {renderPageContent(selectedPage, [], handleUpdateTree, updatePage)}
            <Box
              sx={{
                marginTop: 4,
              }}
            >
              <ButtonGroup variant="contained">
                <Button
                  onClick={() => {
                    updatePage().then(() => {
                      fetchPages();
                    });
                  }}
                >
                  Save
                </Button>
              </ButtonGroup>
            </Box>
          </Grid>

          <Grid item xs={2}>
            <ButtonGroup orientation="vertical" variant="outined">
              {ELEMENTS.map((element, index) => {
                return (
                  <Button
                    draggable
                    key={index}
                    onDragStart={(ev) => {
                      ev.dataTransfer.setData("node", JSON.stringify(element));
                    }}
                  >
                    {element.type}
                  </Button>
                );
              })}
              {contents.map((content, index) => {
                return (
                  <Tooltip
                    title={`Content type: ${content.contentType}, language:  ${content.language}, context: ${content.context}, version: ${content.version}`}
                  >
                    <Button
                      draggable
                      key={content._id}
                      onDragStart={(ev) => {
                        ev.dataTransfer.setData(
                          "node",
                          JSON.stringify({ ...content, type: "content" })
                        );
                      }}
                    >
                      {content.value}
                    </Button>
                  </Tooltip>
                );
              })}
            </ButtonGroup>
          </Grid>
        </Grid>
      </Container>
    </DashboardLayout>
  );
};

export default AppPages;
